from flask import ( 
    Flask, Response, request
)
import json
import redis
import logging as log
import socket
import platform


app = Flask(__name__)
rdb = redis.Redis(host='redis',decode_responses=True)

@app.route("/")
def hello():
    response_obj = { 'host' : socket.getfqdn(), 'ip' : socket.gethostbyname(socket.getfqdn()) , 'python': platform.python_version()}
    return Response(response=json.dumps(response_obj),
                    mimetype="application/json")


@app.route("/api/set")
def set_val():
    for key, val in request.args.items():
        rdb.set(key, val)
    
    response_obj = { 'status' : 'ok' }
    return Response(response=json.dumps(response_obj),
                    mimetype="application/json")


@app.route("/api/get")
def get_val():
    args = request.args
    key = args['key']
    response_obj = {}
    if key:
      val=rdb.get(key)
      response_obj[key]=val
    else:
      response_obj = { 'val' : 'none' }
    
    return Response(response=json.dumps(response_obj),
                    mimetype="application/json")


if __name__ == '__main__':
    app.run(debug=True)


