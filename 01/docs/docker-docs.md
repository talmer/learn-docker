1. Dockerfile 

	https://docs.docker.com/engine/reference/builder/

2. Docker-Compose 

	https://docs.docker.com/compose/compose-file/compose-file-v2/
	https://docs.docker.com/compose/compose-file/compose-file-v3/


3. DockerHub

	https://hub.docker.com/

4. Docker Registry

	https://docs.docker.com/registry/
