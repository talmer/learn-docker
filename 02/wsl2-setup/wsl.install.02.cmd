:: install wsl2 update
curl -o wsl_update_x64.msi https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi
msiexec /i wsl_update_x64.msi
:: set default wsl version 2
wsl --set-default-version 2
:: save wsl help to .txt
wsl -h >wsl.help.txt