# centos:7
image=python
tag=3

docker pull ${image}:${tag}
docker rm ${image}_${tag}
docker create --name ${image}_${tag} -i ${image}:${tag} bash
docker export ${image}_${tag} > ./${image}_${tag}.tar
docker rm ${image}_${tag}
