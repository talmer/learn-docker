set WSL_OS_NAME=Ubuntu-20.04
set WSL_OS_NEW_NAME=ubuntu-dev

set WSL_OS_NEW_PATH=v:/wsl/%WSL_OS_NEW_NAME%/

mkdir %WSL_OS_NEW_PATH%
wsl --import %WSL_OS_NEW_NAME% %WSL_OS_NEW_PATH% .\%WSL_OS_NAME%.tar --version 2

