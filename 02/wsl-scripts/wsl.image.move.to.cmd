set WSL_OS_NAME=Ubuntu-20.04
set WSL_OS_NEW_PATH=W:/wsl/%WSL_OS_NAME%/

wsl --export %WSL_OS_NAME% .\%WSL_OS_NAME%.tar
wsl --unregister %WSL_OS_NAME%
mkdir "%WSL_OS_NEW_PATH%"
wsl --import %WSL_OS_NAME% %WSL_OS_NEW_PATH% .\%WSL_OS_NAME%.tar --version 2
