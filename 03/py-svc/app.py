from flask import ( 
    Flask, Response, request
)
import json
import redis
import logging as log
import socket
import platform
import psycopg2
import os

app = Flask(__name__)
rdb = redis.Redis(host='redis',decode_responses=True)


pgconn = psycopg2.connect(dbname=os.getenv('POSTGRES_DB'), 
                        user=os.getenv('POSTGRES_USER'), 
                        password=os.getenv('POSTGRES_PASSWORD'),
                        host='db')

pgcur = pgconn.cursor()
pgcur.execute('CREATE TABLE IF NOT EXISTS keyval ( key varchar(100) NOT NULL, val varchar(255))')


@app.route("/")
def hello():
    response_obj = { 'host' : socket.getfqdn(), 'ip' : socket.gethostbyname(socket.getfqdn()) , 'python': platform.python_version()}
    return Response(response=json.dumps(response_obj),
                    mimetype="application/json")


@app.route("/api/set")
def set_val():
    for key, val in request.args.items():
        rdb.set(key, val)
    
    response_obj = { 'status' : 'ok' }
    return Response(response=json.dumps(response_obj),
                    mimetype="application/json")



@app.route("/api/setdb")
def set_dbval():
    for key, val in request.args.items():        
        pgcur.execute('INSERT INTO keyval (key,val) values(%s,%s)',(key,val))
        pgconn.commit()

    response_obj = { 'status' : 'ok' }
    return Response(response=json.dumps(response_obj),
                    mimetype="application/json")

@app.route("/api/getdb")
def get_dbval():
    args = request.args
    key = args['key']
    response_obj = {}
    if key:      
      val=''
      pgcur.execute("SELECT * FROM keyval WHERE key='{}'".format(key))
      for row in pgcur:
        log.warning(row)        
        val=row[1]
      response_obj[key]=val
    else:
      response_obj = { 'val' : 'none' }
    
    return Response(response=json.dumps(response_obj),
                    mimetype="application/json")



@app.route("/api/get")
def get_val():
    args = request.args
    key = args['key']
    response_obj = {}
    if key:
      val=rdb.get(key)
      response_obj[key]=val
    else:
      response_obj = { 'val' : 'none' }
    
    return Response(response=json.dumps(response_obj),
                    mimetype="application/json")




if __name__ == '__main__':
    app.run(debug=True)


